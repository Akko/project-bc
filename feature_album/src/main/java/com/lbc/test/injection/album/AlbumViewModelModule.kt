package com.lbc.test.injection.album

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lbc.test.ui.album.AlbumsViewModel
import com.lbc.test.injection.FeatureViewModelFactory
import com.lbc.test.injection.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


/**
 * Created by --J.
 */
@Module
abstract class AlbumViewModelModule {

	@Binds
	@IntoMap
	@ViewModelKey(AlbumsViewModel::class)
	abstract fun provideAlbumsViewModel(viewModel: AlbumsViewModel): ViewModel


	// ===========================================================
	// Factory
	// ===========================================================

	@Binds
	abstract fun provideViewModelFactory(factory: FeatureViewModelFactory): ViewModelProvider.Factory

}