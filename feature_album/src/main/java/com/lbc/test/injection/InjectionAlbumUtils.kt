package com.lbc.test.injection

import android.content.Context
import androidx.fragment.app.Fragment
import com.lbc.test.BaseApplication
import com.lbc.test.injection.album.AlbumFeatureComponent
import com.lbc.test.injection.album.DaggerAlbumFeatureComponent

/**
 * Created by --J.
 */
// Singleton holding instance of component
object AlbumComponent {

	var component: AlbumFeatureComponent? = null

	fun get(context: Context): AlbumFeatureComponent {
		val component = component
		return when (component) {
			null -> {
				val application = context.applicationContext as BaseApplication
				DaggerAlbumFeatureComponent.builder()
						.baseComponent(application.appComponent)
						.build()
						.apply { AlbumComponent.component = this }
			}
			else -> component
		}
	}
}

@Suppress("NOTHING_TO_INLINE")
inline fun Context.getAlbumInjector(): AlbumFeatureComponent = AlbumComponent.get(this)

@Suppress("NOTHING_TO_INLINE")
inline fun Fragment.getAlbumInjector(): AlbumFeatureComponent = AlbumComponent.get(context!!)