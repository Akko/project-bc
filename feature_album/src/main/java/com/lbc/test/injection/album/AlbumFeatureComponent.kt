package com.lbc.test.injection.album

import com.lbc.test.ui.album.AlbumsActivity
import com.lbc.test.injection.BaseComponent
import com.lbc.test.injection.FeatureScope
import dagger.Component


/**
 * Created by --J.
 */
@FeatureScope
@Component(dependencies = [BaseComponent::class],
		modules = [AlbumViewModelModule::class])
interface AlbumFeatureComponent {

	fun inject(activity: AlbumsActivity)

}