package com.lbc.test.ui.album

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lbc.test.album.R
import com.lbc.test.model.entity.Song
import com.lbc.test.model.entity.extension.toTitle
import com.lbc.test.ui.inflate
import com.lbc.test.util.C215


/**
 * Created by --J.
 */
interface SongViewHolder {

	// song
	val songLayout: ViewGroup
	val songTextView: TextView
	val songImageView: ImageView
	var song: Song?

	// onCreateViewHolder
	fun createSong(listener: SongListener) {
		songLayout.setOnClickListener {
			val s = song ?: return@setOnClickListener
			listener.onClickSong(s)
		}
	}

	// onBindViewHolder
	fun bindSong(song: Song?) {
		song ?: return
		bindTitle(song)
		bindImage(song)
		this.song = song
	}

	// onBindViewHolderPayloaded
	fun bindSong(song: Song?, payload: List<*>) {
		song ?: return
		payload.forEach { item ->
			when (item) {
				VH_SONG_TITLE -> bindTitle(song)
				VH_SONG_IMAGE -> bindImage(song)
				else -> Unit
			}
		}
	}


	// ===========================================================
	// Bind
	// ===========================================================

	private fun bindTitle(song: Song) {
		songTextView.text = song.toTitle()
	}


	private fun bindImage(song: Song) {
		songImageView.apply {
			C215.forPictureSmallRadius(url = song.thumbnailUrl,
					imageView = this)
		}
	}


	// ===========================================================
	// ViewHolder
	// ===========================================================

	class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view), SongViewHolder {

		override val songLayout: ViewGroup by lazy { view.findViewById<ViewGroup>(R.id.layout_song) }
		override val songTextView: TextView by lazy { view.findViewById<TextView>(R.id.text_song) }
		override val songImageView: ImageView by lazy { view.findViewById<ImageView>(R.id.image_song) }
		override var song: Song? = null
	}


	// ===========================================================
	// Companion
	// ===========================================================

	companion object {

		const val VH_SONG_TITLE = "vh_song_title"
		const val VH_SONG_IMAGE = "vh_song_image"

		fun inflate(parent: ViewGroup): RecyclerView.ViewHolder {
			val v = parent.inflate(R.layout.fragment_cell_song)
			return ViewHolder(v)
		}

		fun areContentsTheSame(oldItem: Song, newItem: Song) = oldItem.toTitle() == newItem.toTitle()
				&& oldItem.thumbnailUrl == newItem.thumbnailUrl

		fun getChangePayload(oldItem: Song, newItem: Song): ArrayList<String> {
			val changes = ArrayList<String>()
			if (oldItem.toTitle() != newItem.toTitle()) changes.add(VH_SONG_TITLE)
			if (oldItem.thumbnailUrl != newItem.thumbnailUrl) changes.add(VH_SONG_IMAGE)
			return changes
		}
	}
}