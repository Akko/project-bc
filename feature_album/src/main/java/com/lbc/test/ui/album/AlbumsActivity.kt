package com.lbc.test.ui.album

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.transition.TransitionManager
import com.lbc.test.album.R
import com.lbc.test.injection.getAlbumInjector
import com.lbc.test.injection.withActivityLifeCycle
import com.lbc.test.model.api.RError
import com.lbc.test.model.api.RLoading
import com.lbc.test.model.api.RSuccess
import com.lbc.test.model.api.Resource
import com.lbc.test.model.entity.Album
import com.lbc.test.model.entity.Song
import com.lbc.test.ui.doOnScrolled
import com.lbc.test.ui.firstChildTop
import com.lbc.test.ui.snack
import javax.inject.Inject


/**
 * Created by --J.
 */
class AlbumsActivity : AppCompatActivity(), AlbumsAdapter.AlbumsAdapterListener {

	// ===========================================================
	// Property
	// ===========================================================

	// VM
	@Inject
	@Suppress("MemberVisibilityCanPrivate") // cannot be, cause not constructor injection
	lateinit var mViewModelFactory: ViewModelProvider.Factory
	private val albumsVM: AlbumsViewModel by lazy { withActivityLifeCycle<AlbumsViewModel>(mViewModelFactory) }

	// view
	private val rootLayout: ViewGroup by lazy { findViewById<ViewGroup>(R.id.layout_root) }
	private val contentLayout: ViewGroup by lazy { findViewById<ViewGroup>(R.id.layout_content) }
	private val refreshLayout: SwipeRefreshLayout by lazy { findViewById<SwipeRefreshLayout>(R.id.layout_refresh) }

	// error / empty
	private val errorTitleTextView: TextView by lazy { findViewById<TextView>(R.id.text_error_title) }
	private val errorSubtitleTextView: TextView by lazy { findViewById<TextView>(R.id.text_error_subtitle) }
	private val emptyTitleTextView: TextView by lazy { findViewById<TextView>(R.id.text_empty_title) }
	private val noContentImageView: ImageView by lazy { findViewById<ImageView>(R.id.image_no_content) }
	private val retryTextView: Button by lazy { findViewById<Button>(R.id.text_error_retry) }

	// loading
	private val loadingProgressbar: ProgressBar by lazy { findViewById<ProgressBar>(R.id.progress_loading) }

	// content
	private val recyclerView: RecyclerView by lazy {
		findViewById<RecyclerView>(R.id.recyclerview).apply {
			setupRecyclerView(this)
		}
	}
	private val adapter: AlbumsAdapter by lazy { AlbumsAdapter(listener = this) }


	// ===========================================================
	// Life Cycle
	// ===========================================================

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_albums)
		getAlbumInjector().inject(this)

		albumsVM.albums.observe(this, Observer { resource ->
			resource ?: return@Observer
			val data = resource.data
			TransitionManager.beginDelayedTransition(contentLayout)
			if (data?.isNotEmpty() == true) {
				switchContent()
				calculateDiffAndPush(resource)
			} else {
				// empty content, adjust screen to show
				when (resource.status) {
					RSuccess -> switchEmpty()
					RLoading -> switchLoading()
					RError -> switchError()
				}
			}
		})
		retryTextView.setOnClickListener { albumsVM.requestFirst() }
		albumsVM.errors.observe(this, Observer { error ->
			// if adapter is empty -> error view, do nothings
			// else -> snack it
			if (adapter.itemCount != 0) snack(rootLayout, error)
		})
	}


	// ===========================================================
	// Menu
	// ===========================================================

	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		when (item?.itemId) {
			android.R.id.home -> {
				finish()
				return true
			}
		}
		return super.onOptionsItemSelected(item)
	}


	// ===========================================================
	// Setup
	// ===========================================================

	private fun setupRecyclerView(recyclerView: RecyclerView) = with(recyclerView) {
		adapter = this@AlbumsActivity.adapter
		val lm = LinearLayoutManager(this@AlbumsActivity, RecyclerView.VERTICAL, false)
		layoutManager = lm
		doOnScrolled { _, _, _ ->
			val enable = firstChildTop() >= 0
			refreshLayout.isEnabled = enable && lm.findFirstVisibleItemPosition() == 0
		}
		// next request
		doOnScrolled { _, _, dy ->
			//check for scroll down
			if (dy > 0) {
				val visibleItemCount = lm.childCount
				val totalItemCount = lm.itemCount
				val pastVisibleItems = lm.findFirstVisibleItemPosition()

				if ((visibleItemCount + pastVisibleItems) >= totalItemCount - 5) {
					albumsVM.requestNext(getLastAlbumId())
				}
			}
		}
	}


	// ===========================================================
	// Update
	// ===========================================================

	private fun calculateDiffAndPush(resource: Resource<List<Album>>) {
		val oldList = adapter.items
		val newList = AlbumsAdapter.generateItems(resource)

		// compare and push diff
		val diffResult = DiffUtil.calculateDiff(AlbumDiffCallback(oldList, newList))
		diffResult.dispatchUpdatesTo(adapter)
		adapter.items = newList
	}


	// ===========================================================
	// Adapter
	// ===========================================================

	override fun onClickSong(song: Song) {
		onClickSongImpl(this, song)
	}

	override fun onClickError() {
		albumsVM.requestNext(getLastAlbumId())
	}

	private fun getLastAlbumId(): Int? = (adapter
			.items
			.lastOrNull { (it as? Album)?.albumId != null } as? Album)
			?.albumId


	// ===========================================================
	// Switch
	// ===========================================================

	private fun switchError() {
		// error case
		errorTitleTextView.visibility = View.VISIBLE
		errorSubtitleTextView.visibility = View.VISIBLE
		retryTextView.visibility = View.VISIBLE
		emptyTitleTextView.visibility = View.GONE
		noContentImageView.visibility = View.VISIBLE

		loadingProgressbar.visibility = View.GONE

		recyclerView.visibility = View.GONE

		refreshLayout.visibility = View.VISIBLE
		refreshLayout.isEnabled = true
	}

	private fun switchEmpty() {
		// empty case
		errorTitleTextView.visibility = View.GONE
		errorSubtitleTextView.visibility = View.GONE
		retryTextView.visibility = View.GONE
		emptyTitleTextView.visibility = View.VISIBLE
		noContentImageView.visibility = View.VISIBLE

		loadingProgressbar.visibility = View.GONE

		recyclerView.visibility = View.GONE

		refreshLayout.visibility = View.VISIBLE
		refreshLayout.isEnabled = true
	}

	private fun switchContent() {
		// content case
		errorTitleTextView.visibility = View.GONE
		errorSubtitleTextView.visibility = View.GONE
		retryTextView.visibility = View.GONE
		emptyTitleTextView.visibility = View.GONE
		noContentImageView.visibility = View.GONE

		loadingProgressbar.visibility = View.GONE

		recyclerView.visibility = View.VISIBLE

		refreshLayout.visibility = View.VISIBLE
		refreshLayout.isEnabled = true
	}

	private fun switchLoading() {
		// loading case
		errorTitleTextView.visibility = View.GONE
		errorSubtitleTextView.visibility = View.GONE
		retryTextView.visibility = View.GONE
		emptyTitleTextView.visibility = View.GONE
		noContentImageView.visibility = View.GONE

		loadingProgressbar.visibility = View.VISIBLE

		recyclerView.visibility = View.GONE

		refreshLayout.visibility = View.GONE
		refreshLayout.isEnabled = false
	}
}