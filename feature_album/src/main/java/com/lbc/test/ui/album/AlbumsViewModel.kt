package com.lbc.test.ui.album

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.lbc.test.model.api.AppExecutors
import com.lbc.test.model.api.RLoading
import com.lbc.test.model.api.Resource
import com.lbc.test.model.entity.Album
import com.lbc.test.model.entity.Error
import com.lbc.test.model.repository.AlbumRepository
import com.lbc.test.model.repository.expand.AlbumRepoExpand
import javax.inject.Inject

/**
 * Created by --J.
 */
class AlbumsViewModel
@Inject constructor(
		private val albumRepo: AlbumRepository,
		private val appExecutors: AppExecutors
) : ViewModel() {

	// ===========================================================
	// Property
	// ===========================================================

	// data
	private var request: Boolean = false
	private var currentAlbums: ArrayList<Album> = ArrayList()

	// VM
	private val expand = AlbumRepoExpand.EXPAND_TYPE_ALBUMS
	val errors: MutableLiveData<Error> = MediatorLiveData()
	val albums: MutableLiveData<Resource<List<Album>>> = MediatorLiveData()


	// ===========================================================
	// Observer
	// ===========================================================

	private val observerAlbums: Observer<Resource<List<Album>>> by lazy {
		Observer<Resource<List<Album>>> { resource ->
			if (resource?.expand != expand) return@Observer
			// first page asked, clear list
			if (resource.page == null) {
				currentAlbums.clear()
			}

			// update VM data
			resource.data?.let { data -> currentAlbums.addAll(data) }

			mainThread {
				// push old + new albums
				// duplicate list :
				// this new one will be set to adapter
				// the above one is used to contains all items
				val adapterList = ArrayList(currentAlbums)
				albums.value = Resource.fromResource(resource, adapterList)
				// push error if present
				resource.error?.let { error ->
					errors.value = error
					errors.value = null
				}
				request = resource.status == RLoading
			}
		}
	}


	// ===========================================================
	// View Model
	// ===========================================================

	override fun onCleared() {
		albumRepo.observeAlbums().removeObserver(observerAlbums)
	}

	init {
		albumRepo.observeAlbums().observeForever(observerAlbums)

		requestFirst()
	}


	// ===========================================================
	// Request
	// ===========================================================

	fun requestFirst() {
		albumRepo.getAlbums(expand = expand)
	}

	fun requestNext(lastAlbumId: Int?) {
		lastAlbumId ?: return
		// this is the end ?
		val end = albums.value?.end ?: true
		if (end) return
		if (request) return
		request = true
		albumRepo.getAlbums(lastAlbumId, expand = expand)
	}


	// ===========================================================
	// Extension
	// ===========================================================

	@Suppress("unused")
	private fun AlbumsViewModel.mainThread(executable: () -> Unit) = appExecutors.mainThread.execute(executable)
}