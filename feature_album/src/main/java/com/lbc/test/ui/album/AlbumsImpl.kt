package com.lbc.test.ui.album

import android.content.Context
import android.widget.Toast
import com.lbc.test.album.R
import com.lbc.test.model.entity.Song

/**
 * Created by --J.
 */
// ===========================================================
// Interface
// ===========================================================

interface SongListener {

	fun onClickSong(song: Song)
}

fun onClickSongImpl(context: Context, song: Song) {
	val text = context.getString(R.string.toast_playing_x, song.title ?: "")
	Toast.makeText(context, text, Toast.LENGTH_LONG).show()

}