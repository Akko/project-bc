package com.lbc.test.ui.album

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lbc.test.album.R
import com.lbc.test.model.entity.Album
import com.lbc.test.model.entity.extension.toTitle
import com.lbc.test.ui.inflate


/**
 * Created by --J.
 */
interface AlbumViewHolder {

	val albumTextView: TextView


	// onBindViewHolder
	fun bindAlbum(album: Album?) {
		album ?: return
		bindTitle(album)
	}

	// onBindViewHolderPayloaded
	fun bindAlbum(album: Album?, payload: List<*>) {
		album ?: return
		payload.forEach { item ->
			when (item) {
				VH_ALBUM_TITLE -> bindTitle(album)
				else -> Unit
			}
		}
	}


	// ===========================================================
	// Bind
	// ===========================================================

	private fun bindTitle(album: Album) {
		albumTextView.text = album.toTitle()
	}


	// ===========================================================
	// ViewHolder
	// ===========================================================

	class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view), AlbumViewHolder {
		override val albumTextView: TextView by lazy { view.findViewById<TextView>(R.id.text_album) }
	}


	// ===========================================================
	// Companion
	// ===========================================================

	companion object {

		const val VH_ALBUM_TITLE = "vh_album_title"

		fun inflate(parent: ViewGroup): RecyclerView.ViewHolder {
			val v = parent.inflate(R.layout.fragment_cell_album)
			return ViewHolder(v)
		}

		fun areContentsTheSame(oldItem: Album, newItem: Album) = oldItem.toTitle() == newItem.toTitle()

		fun getChangePayload(oldItem: Album, newItem: Album): ArrayList<String> {
			val changes = ArrayList<String>()
			if (oldItem.toTitle() != newItem.toTitle()) changes.add(VH_ALBUM_TITLE)
			return changes
		}
	}
}