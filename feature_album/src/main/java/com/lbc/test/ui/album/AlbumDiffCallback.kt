package com.lbc.test.ui.album

import androidx.recyclerview.widget.DiffUtil
import com.lbc.test.model.entity.Album
import com.lbc.test.model.entity.Song
import com.lbc.test.model.entity.list.ErrorCell
import com.lbc.test.model.entity.list.HappyEndCell
import com.lbc.test.model.entity.list.LoadCell

/**
 * Created by --J.
 */
class AlbumDiffCallback(
		private val oldList: List<Any>,
		private val newList: List<Any>
) : DiffUtil.Callback() {

	override fun getOldListSize() = oldList.size
	override fun getNewListSize() = newList.size

	override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
		val oldItem = oldList[oldItemPosition]
		val newItem = newList[newItemPosition]
		if (oldItem is Album && newItem is Album) {
			return oldItem.albumId == newItem.albumId
		}
		if (oldItem is Song && newItem is Song) {
			return oldItem.songId == newItem.songId
		}
		if (oldItem is Int && newItem is Int) {
			// dividers ara attached between cells, compare previous and next views
			return when {
				// compare previous one and next one
				oldItemPosition > 0 && newItemPosition > 0
						&& oldItemPosition < (oldListSize - 2)
						&& newItemPosition < (newListSize - 2)
				-> areItemsTheSame(oldItemPosition - 1, newItemPosition - 1)
						&& areItemsTheSame(oldItemPosition + 1, newItemPosition + 1)
				// no previous one, so check if same divider
				else -> oldItem == newItem
			}
		}
		if (oldItem is LoadCell && newItem is LoadCell) {
			return true
		}
		if (oldItem is HappyEndCell && newItem is HappyEndCell) {
			return true
		}
		if (oldItem is ErrorCell && newItem is ErrorCell) {
			return true
		}
		return false
	}

	override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
		val oldItem = oldList[oldItemPosition]
		val newItem = newList[newItemPosition]
		if (oldItem is Album && newItem is Album) {
			return AlbumViewHolder.areContentsTheSame(oldItem, newItem)
		}
		if (oldItem is Song && newItem is Song) {
			return SongViewHolder.areContentsTheSame(oldItem, newItem)
		}
		if (oldItem is Int && newItem is Int) {
			// dividers are already compared each other above
			return true
		}
		return false
	}

	override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
		val oldItem = oldList[oldItemPosition]
		val newItem = newList[newItemPosition]
		when {
			oldItem is Song && newItem is Song -> return SongViewHolder.getChangePayload(oldItem, newItem)
			oldItem is Album && newItem is Album -> return AlbumViewHolder.getChangePayload(oldItem, newItem)
		}
		return super.getChangePayload(oldItemPosition, newItemPosition)
	}
}