package com.lbc.test.ui.album

import android.view.ViewGroup
import androidx.annotation.IntDef
import androidx.recyclerview.widget.RecyclerView
import com.lbc.test.model.api.RError
import com.lbc.test.model.api.RLoading
import com.lbc.test.model.api.RSuccess
import com.lbc.test.model.api.Resource
import com.lbc.test.model.entity.Album
import com.lbc.test.model.entity.Song
import com.lbc.test.model.entity.list.ErrorCell
import com.lbc.test.model.entity.list.HappyEndCell
import com.lbc.test.model.entity.list.LoadCell
import com.lbc.test.ui.ADAPTER_TYPE_DIVIDER_FULL
import com.lbc.test.ui.ADAPTER_TYPE_DIVIDER_MARGIN
import com.lbc.test.ui.inflateDividerFull
import com.lbc.test.ui.inflateDividerMargin
import com.lbc.test.ui.list.ErrorViewHolder
import com.lbc.test.ui.list.HappyEndViewHolder
import com.lbc.test.ui.list.LoadViewHolder


/**
 * Created by --J.
 */
class AlbumsAdapter(
		var items: ArrayList<Any> = ArrayList(),
		private val listener: AlbumsAdapterListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

	// ===========================================================
	// Interface
	// ===========================================================

	interface AlbumsAdapterListener : SongListener,
			ErrorViewHolder.ErrorListener


	// ===========================================================
	// Companion
	// ===========================================================

	companion object {

		// ===========================================================
		// Albums Cell
		// ===========================================================

		@IntDef(ADAPTER_TYPE_ALBUM, ADAPTER_TYPE_SONG,
				ADAPTER_TYPE_LOAD, ADAPTER_TYPE_HAPPY_END, ADAPTER_TYPE_ERROR,
				ADAPTER_TYPE_DIVIDER_FULL, ADAPTER_TYPE_DIVIDER_MARGIN)
		@Retention(AnnotationRetention.SOURCE)
		@Suppress("unused")
		internal annotation class AlbumsCell

		const val ADAPTER_TYPE_ALBUM = 0x1
		const val ADAPTER_TYPE_SONG = 0x2

		const val ADAPTER_TYPE_LOAD = 0x11
		const val ADAPTER_TYPE_HAPPY_END = 0x12
		const val ADAPTER_TYPE_ERROR = 0x13

		@JvmStatic
		fun generateItems(resource: Resource<List<Album>>?) = ArrayList<Any>().apply {
			val albums = resource?.data ?: return@apply
			if (albums.isEmpty()) return@apply
			albums.forEach { album ->
				add(album)
				add(ADAPTER_TYPE_DIVIDER_FULL)
				val songs = album.songs
				if (songs?.isNotEmpty() == true) {
					songs.forEach { song ->
						add(song)
						add(ADAPTER_TYPE_DIVIDER_MARGIN)
					}
					val indexMargin = lastIndexOf(ADAPTER_TYPE_DIVIDER_MARGIN)
					if (indexMargin != -1) removeAt(indexMargin)
				}
				add(ADAPTER_TYPE_DIVIDER_FULL)
			}

			when (resource.status) {
				RLoading -> {
					add(LoadCell())
					add(ADAPTER_TYPE_DIVIDER_FULL)
				}
				RError -> {
					add(ErrorCell())
					add(ADAPTER_TYPE_DIVIDER_FULL)
				}
				RSuccess -> {
					if (!resource.end) {
						add(LoadCell())
						add(ADAPTER_TYPE_DIVIDER_FULL)
					} else {
						add(HappyEndCell())
						add(ADAPTER_TYPE_DIVIDER_FULL)
					}
				}
			}
			val indexMargin = lastIndexOf(ADAPTER_TYPE_DIVIDER_FULL)
			if (indexMargin != -1) removeAt(indexMargin)
		}
	}


	// ===========================================================
	// Adapter
	// ===========================================================

	override fun getItemViewType(position: Int): Int {
		val item = items[position]
		return when (item) {
			is Album -> ADAPTER_TYPE_ALBUM
			is Song -> ADAPTER_TYPE_SONG
			is HappyEndCell -> ADAPTER_TYPE_HAPPY_END
			is LoadCell -> ADAPTER_TYPE_LOAD
			is ErrorCell -> ADAPTER_TYPE_ERROR
			ADAPTER_TYPE_DIVIDER_FULL -> ADAPTER_TYPE_DIVIDER_FULL
			ADAPTER_TYPE_DIVIDER_MARGIN -> ADAPTER_TYPE_DIVIDER_MARGIN
			else -> super.getItemViewType(position)
		}
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
		val h = when (viewType) {
			ADAPTER_TYPE_ALBUM -> AlbumViewHolder.inflate(parent)
			ADAPTER_TYPE_SONG -> SongViewHolder.inflate(parent)
			ADAPTER_TYPE_HAPPY_END -> HappyEndViewHolder.inflate(parent)
			ADAPTER_TYPE_LOAD -> LoadViewHolder.inflate(parent)
			ADAPTER_TYPE_ERROR -> ErrorViewHolder.inflate(parent)
			ADAPTER_TYPE_DIVIDER_FULL -> inflateDividerFull(parent)
			ADAPTER_TYPE_DIVIDER_MARGIN -> inflateDividerMargin(parent)
			else -> throw IllegalStateException("End of adapter reach ${javaClass.simpleName} for viewType $viewType")
		}
		(h as? ErrorViewHolder)?.createError(listener)
		(h as? SongViewHolder)?.createSong(listener)
		return h
	}

	override fun onBindViewHolder(h: RecyclerView.ViewHolder, position: Int) {
		val item = items[position]
		(h as? SongViewHolder)?.bindSong(item as? Song)
		(h as? AlbumViewHolder)?.bindAlbum(item as? Album)
	}

	override fun onBindViewHolder(h: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
		if (payloads.isEmpty()) {
			onBindViewHolder(h, position)
		} else {
			payloads.forEach { payload ->
				when (payload) {
					is List<*> -> {
						val item = items[position]
						(h as? AlbumViewHolder)?.bindAlbum(item as? Album, payload)
						(h as? SongViewHolder)?.bindSong(item as? Song, payload)
					}
				}
			}
		}
	}


	override fun getItemCount() = items.size
}