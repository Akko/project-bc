package com.lbc.test.model.api

import com.lbc.test.model.entity.Error
import com.lbc.test.util.logMessage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


/**
 * Created by --J.
 */
class ApiClient {

	fun <T> postCall(call: Call<T>, callback: LBCCallback<T>) {
		call.enqueue(wrapCallback(callback))
	}

	private fun <T> wrapCallback(callWrapped: LBCCallback<T>): Callback<T> {
		return object : Callback<T> {

			override fun onResponse(call: Call<T>?, response: Response<T>?) {
				val body = response?.body()
				if (body == null || call == null) {
					callWrapped.onFailure(Error().apply {
						type = Error.ERROR_TYPE_NETWORK
					})
					return
				}
				if (response.isSuccessful) {
					callWrapped.onResponse(body, call)
					return
				}
				// Somethings went wrong, push code (and message if present)
				callWrapped.onFailure(Error().apply {
					this.type = Error.ERROR_TYPE_API
					this.code = response.code()
				})
			}

			override fun onFailure(call: Call<T>?, t: Throwable?) {
				val error = Error()
				if (t is IOException) {
					// this is an actual network failure :( inform the user and possibly retry
					// logging probably not necessary
					error.type = Error.ERROR_TYPE_NETWORK
					callWrapped.onFailure(error)
				} else {
					// log to some central bug tracking service
					// conversion issue! big problems :(
					logMessage(t?.message)
					error.type = Error.ERROR_TYPE_UNKNOWN
					error.type = t?.message
					callWrapped.onFailure(error)
				}
			}
		}
	}
}