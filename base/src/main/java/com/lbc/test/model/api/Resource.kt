package com.lbc.test.model.api

import com.lbc.test.model.entity.Error
import com.lbc.test.model.repository.expand.Expand


/**
 * Created by --J.
 */
class Resource<out T> constructor(
		val status: RStatus,
		val data: T? = null,
		var parameter: Any? = null,
		var page: Int? = null,
		var expand: Expand? = null,
		var end: Boolean = false,
		val error: Error? = null
) {

	companion object {

		@JvmStatic
		fun <T> success(data: T?): Resource<T> {
			return Resource(RSuccess, data = data)
		}

		@JvmStatic
		fun <T> error(data: T?, error: Error): Resource<T> {
			return Resource(RError, data = data, error = error)
		}

		@JvmStatic
		fun <T> loading(data: T?): Resource<T> {
			return Resource(RLoading, data = data)
		}

		@JvmStatic
		fun <T> fromResource(resource: Resource<T>, newData: T): Resource<T> {
			return Resource(status = resource.status,
					data = newData,
					parameter = resource.parameter,
					page = resource.page,
					expand = resource.expand,
					end = resource.end,
					error = resource.error)
		}
	}
}