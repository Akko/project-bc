package com.lbc.test.model.api

import com.lbc.test.model.entity.Album
import com.lbc.test.model.entity.Song
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import retrofit2.Converter
import retrofit2.Retrofit
import java.io.IOException
import java.lang.reflect.Type


/**
 * Created by --J.
 */
object JsonConverterFactory : Converter.Factory(), Converter<ResponseBody, Any> {

	override fun responseBodyConverter(type: Type?, annotations: Array<Annotation>?, retrofit: Retrofit?): Converter<ResponseBody, *> {
		return this
	}

	@Throws(IOException::class)
	override fun convert(responseBody: ResponseBody): Any {
		try {
			val response: String = responseBody.string().trim()
			var currentAlbum: Album? = null
			val albums = ArrayList<Album>()

			// json array
			val json = JSONArray(response)
			for (i in 0 until json.length()) {
				val jObject = json.getJSONObject(i)
				val song = Song(jObject)

				// hacky -> album
				if (currentAlbum?.albumId != song.albumId) {
					currentAlbum = albums.firstOrNull { it.albumId == song.albumId } ?: (Album(song.albumId).apply { albums.add(this) })
				}

				// FIXME: hacky -> attach songs to album
				currentAlbum.songs = (currentAlbum.songs as? ArrayList
						?: ArrayList()).apply { add(song) }
				// hacky -> attach parent
				song.album = currentAlbum
			}

			// hacky -> attach child ids
			albums.forEach { album ->
				album.songIds = album.songs?.mapNotNull { it.songId }
			}
			return albums
		} catch (e: JSONException) {
			throw IOException("Failed to parse SUCCESS_JSON", e)
		}
	}
}
