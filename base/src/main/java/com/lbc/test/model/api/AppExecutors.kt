package com.lbc.test.model.api

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject


/**
 * Created by --J.
 */
class AppExecutors @Inject constructor() {

	// used for DB transaction
	val diskIO: Executor = Executors.newSingleThreadExecutor()
	// used for Network operation
	val networkIO: Executor = Executors.newFixedThreadPool(3)
	// used to push LiveData updates
	val mainThread: Executor = MainThreadExecutor()

	private class MainThreadExecutor : Executor {

		private val mainThreadHandler = Handler(Looper.getMainLooper())

		override fun execute(command: Runnable) {
			mainThreadHandler.post(command)
		}
	}
}
