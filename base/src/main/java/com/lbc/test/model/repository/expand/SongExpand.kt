package com.lbc.test.model.repository.expand


/**
 * Created by --J.
 */
class SongExpand(
		override val name: String = SONGS,
		override val limit: Int? = null
) : Expand {

	companion object {
		const val SONGS = "songs"
	}

	override val list: ArrayList<Expand?> = arrayListOf()
}