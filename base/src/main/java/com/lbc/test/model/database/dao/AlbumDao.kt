package com.lbc.test.model.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.lbc.test.LIMIT_PAGE
import com.lbc.test.model.entity.Album
import com.lbc.test.model.entity.Song


/**
 * Created by --J.
 */
@Dao
interface AlbumDao : BaseDao<Album> {

	// ===========================================================
	// Query
	// ===========================================================

	@Query("SELECT * FROM ${Album.DB_TABLE_NAME} ORDER BY ${Album.DB_KEY_ID} LIMIT $LIMIT_PAGE")
	fun findAlbums(): List<Album>?

	@Query("SELECT * FROM ${Album.DB_TABLE_NAME} WHERE ${Album.DB_KEY_ID} > :last_album_id ORDER BY ${Album.DB_KEY_ID} LIMIT $LIMIT_PAGE")
	fun findAlbums(last_album_id: Int): List<Album>?

	@Query("DELETE FROM ${Album.DB_TABLE_NAME}")
	fun nukeAll()
}