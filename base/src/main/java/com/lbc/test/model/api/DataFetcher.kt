package com.lbc.test.model.api

import androidx.lifecycle.MediatorLiveData
import com.lbc.test.model.entity.Error
import retrofit2.Call


/**
 * Created by --J.
 */
abstract class DataFetcher<T>(private val appExecutors: AppExecutors,
                              private val apiClient: ApiClient,
                              private val liveData: MediatorLiveData<Resource<T>>) {

	// ===========================================================
	// Property
	// ===========================================================

	var dataDB: T? = null


	// ===========================================================
	// Fetcher
	// ===========================================================

	init {
		start()
	}

	private fun start() {
		diskIO {
			// DB
			dataDB = fetch()

			// hacky things, call normally shouldn't be null for paginate a feed
			// if null -> use this condition as a success and not request feed
			// else -> return a loading resource and call api
			val call = getCall()
			val resourceLoading = if (call == null) Resource.success(data = dataDB) else Resource.loading(data = dataDB)
			completeResource(resourceLoading, dataDB)

			// UI
			mainThread {
				liveData.value = resourceLoading
				liveData.value = null
			}


			call ?: return@diskIO
			networkIO {
				// CALL
				apiClient.postCall(call, object : LBCCallback<T> {

					override fun onResponse(response: T, call: Call<T>) {
						diskIO {
							// DB update
							upsert(response)

							// if db has no data, return feed response
							val resourceSuccess = Resource.success(data = response)
							completeResource(resourceSuccess, response)

							// UI
							mainThread {
								liveData.value = resourceSuccess
								liveData.value = null
							}
						}
					}

					override fun onFailure(error: Error) {
						diskIO {
							// Resource return
							val resourceError = Resource.error(data = dataDB, error = error)
							completeResource(resourceError, dataDB)

							// UI
							mainThread {
								liveData.value = resourceError
								liveData.value = null
							}
						}
					}
				})
			}
		}
	}

	abstract fun upsert(response: T)

	abstract fun fetch(): T?

	abstract fun completeResource(resource: Resource<T>, data: T?)

	abstract fun getCall(): Call<T>?


	// ===========================================================
	// Extension
	// ===========================================================

	@Suppress("unused")
	private fun DataFetcher<T>.networkIO(executable: () -> Unit) = appExecutors.networkIO.execute(executable)

	@Suppress("unused")
	private fun DataFetcher<T>.diskIO(executable: () -> Unit) = appExecutors.diskIO.execute(executable)

	@Suppress("unused")
	private fun DataFetcher<T>.mainThread(executable: () -> Unit) = appExecutors.mainThread.execute(executable)
}