package com.lbc.test.model.api

import com.lbc.test.model.entity.Error
import retrofit2.Call


/**
 * Created by --J.
 */
interface LBCCallback<T> {

	fun onResponse(response: T, call: Call<T>)

	fun onFailure(error: Error)
}