package com.lbc.test.model.entity


/**
 * Created by --J.
 */
class Error {

	// ===========================================================
	// Companion
	// ===========================================================

	companion object {

		// VALUE TYPE
		const val ERROR_TYPE_UNKNOWN: String = "unknown"
		const val ERROR_TYPE_NETWORK: String = "network"
		const val ERROR_TYPE_API: String = "api"
	}


	// ===========================================================
	// Property
	// ===========================================================

	var code: Int? = null
	var type: String? = null

}