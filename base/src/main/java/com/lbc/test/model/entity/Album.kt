package com.lbc.test.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey


/**
 * Created by --J.
 */
@Entity(tableName = Album.DB_TABLE_NAME)
class Album {

	// ===========================================================
	// Companion
	// ===========================================================

	companion object {

		// DB KEY
		const val DB_TABLE_NAME = "album"
		const val DB_KEY_ID = "album_id"

		const val DB_KEY_SONG_IDS = "song_ids"
	}


	// ===========================================================
	// Property
	// ===========================================================

	@PrimaryKey
	@ColumnInfo(name = DB_KEY_ID)
	var albumId: Int? = null

	@Ignore
	var songs: List<Song>? = null
	@ColumnInfo(name = DB_KEY_SONG_IDS)
	var songIds: List<Int>? = null


	// ===========================================================
	// Constructor
	// ===========================================================

	@Suppress("unused") // used by ROOM
	constructor()

	@Ignore
	constructor(albumId: Int) {
		this.albumId = albumId
	}
}