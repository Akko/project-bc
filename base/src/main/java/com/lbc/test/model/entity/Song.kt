package com.lbc.test.model.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.lbc.test.util.getJSONInt
import com.lbc.test.util.getJSONString
import org.json.JSONObject


/**
 * Created by --J.
 */
@Entity(tableName = Song.DB_TABLE_NAME)
class Song {

	// ===========================================================
	// Companion
	// ===========================================================

	companion object {

		// WS KEY
		const val WS_KEY_ID: String = "id"

		const val WS_KEY_ALBUM_ID: String = "albumId"
		const val WS_KEY_TITLE: String = "title"
		const val WS_KEY_URL: String = "url"
		const val WS_KEY_THUMBNAIL_URL: String = "thumbnailUrl"

		// DB KEY
		const val DB_TABLE_NAME = "song"
		const val DB_KEY_ID = "song_id"

		const val DB_KEY_ALBUM_ID: String = "album_id"
		const val DB_KEY_TITLE: String = "title"
		const val DB_KEY_URL: String = "url"
		const val DB_KEY_THUMBNAIL_URL: String = "thumbnail_url"
	}


	// ===========================================================
	// Property
	// ===========================================================

	@PrimaryKey
	@ColumnInfo(name = DB_KEY_ID)
	var songId: Int? = null


	@ColumnInfo(name = DB_KEY_TITLE)
	var title: String? = null
	@ColumnInfo(name = DB_KEY_URL)
	var url: String? = null
	@ColumnInfo(name = DB_KEY_THUMBNAIL_URL)
	var thumbnailUrl: String? = null

	@ColumnInfo(name = DB_KEY_ALBUM_ID)
	var albumId: Int = 0
	@Ignore
	var album: Album? = null


	// ===========================================================
	// Constructor
	// ===========================================================

	@Suppress("unused") // used by ROOM
	constructor()

	@Ignore
	constructor(json: JSONObject) {
		songId = json.getJSONInt(WS_KEY_ID)

		albumId = json.getJSONInt(WS_KEY_ALBUM_ID) ?: 0

		title = json.getJSONString(WS_KEY_TITLE)
		url = json.getJSONString(WS_KEY_URL)
		thumbnailUrl = json.getJSONString(WS_KEY_THUMBNAIL_URL)
	}
}