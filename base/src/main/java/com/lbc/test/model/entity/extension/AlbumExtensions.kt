package com.lbc.test.model.entity.extension

import com.lbc.test.model.entity.Album
import com.lbc.test.util.isDebug


/**
 * Created by --J.
 */
fun Album.toTitle(): String = "$albumId${if (isDebug()) " ($albumId)" else ""}"