package com.lbc.test.model.api


/**
 * Created by --J.
 */
sealed class RStatus

object RSuccess : RStatus()

object RError : RStatus()

object RLoading : RStatus()
