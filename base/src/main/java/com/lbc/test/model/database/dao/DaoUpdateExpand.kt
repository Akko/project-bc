package com.lbc.test.model.database.dao

import androidx.room.Transaction
import com.lbc.test.model.entity.Album
import com.lbc.test.model.repository.expand.Expand
import com.lbc.test.model.repository.expand.SongExpand


/**
 * Created by --J.
 */
// ===========================================================
// Album
// ===========================================================

@Transaction
fun updateAlbumsAndDescendant(values: List<Album>?,
                              albumDao: AlbumDao,
                              songDao: SongDao,
                              expands: Expand) {
	values ?: return
	// FIXME: one feed to rule them all
	// nuke all of them
	// #hacky
	nuke(albumDao, songDao)
	expands.list.forEach { expand ->
		when (expand) {
			is SongExpand -> {
				// songs
				values.mapNotNull { it.songs }
						.forEach { songs -> songDao.inserts(*songs.toTypedArray()) }
			}
		}
	}
	// albums
	albumDao.inserts(*values.toTypedArray())
}

@Transaction
fun nuke(albumDao: AlbumDao, songDao: SongDao) {
	albumDao.nukeAll()
	songDao.nukeAll()
}