package com.lbc.test.model.database

import androidx.room.TypeConverter


/**
 * Created by --J.
 */
class DatabaseConverters {

	@TypeConverter
	fun fromList(value: List<Int>?): String? {
		return value?.joinToString(",")
	}

	@TypeConverter
	fun fromString(date: String?): List<Int>? {
		return date?.split(",")?.mapNotNull { it.toIntOrNull() }
	}
}
