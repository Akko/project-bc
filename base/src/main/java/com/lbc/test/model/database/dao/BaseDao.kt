package com.lbc.test.model.database.dao

import androidx.room.Insert
import androidx.room.OnConflictStrategy


/**
 * Created by --J.
 */
interface BaseDao<T> {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insert(obj: T)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun inserts(vararg obj: T)
}