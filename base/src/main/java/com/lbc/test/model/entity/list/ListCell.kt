package com.lbc.test.model.entity.list

/**
 * Created by --J.
 */
sealed class ListCell

class LoadCell : ListCell()

class ErrorCell : ListCell()

class HappyEndCell : ListCell()

