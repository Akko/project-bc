package com.lbc.test.model.util

import com.lbc.test.model.entity.Album
import com.lbc.test.model.entity.Song
import java.util.*


/**
 * Created by --J.
 */
object AlbumSort {

	fun byAlbumSong(albums: ArrayList<Album>) = with(albums) {
		sortedBy(Album::albumId)
		forEach {
			it.songs = it.songs?.toMutableList()?.apply { sortBy(Song::songId) }
		}
	}
}