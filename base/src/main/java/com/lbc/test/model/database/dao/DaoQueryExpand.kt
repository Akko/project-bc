package com.lbc.test.model.database.dao

import androidx.room.Transaction
import com.lbc.test.model.entity.Album
import com.lbc.test.model.repository.expand.Expand
import com.lbc.test.model.repository.expand.SongExpand


/**
 * Created by --J.
 */
// ===========================================================
// Album
// ===========================================================

@Transaction
fun queryAlbums(
		lastAlbumId: Int?,
		albumDao: AlbumDao,
		songDao: SongDao,
		expands: Expand
): List<Album>? {
	// albums
	val albums = if (lastAlbumId == null) albumDao.findAlbums() else albumDao.findAlbums(lastAlbumId)
	expands.list.forEach { expand ->
		when (expand) {
			is SongExpand -> {
				albums?.forEach { album ->
					album.songs = songDao.findSongsBySongIds(album.songIds)
				}
			}
		}
	}
	return albums
}
