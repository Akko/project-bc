package com.lbc.test.model.service

import com.lbc.test.model.entity.Album
import retrofit2.Call
import retrofit2.http.GET


/**
 * Created by --J.
 */
interface AlbumService {

	// ===========================================================
	// Abuse
	// ===========================================================

	@GET("/technical-test.json")
	fun getAlbums(): Call<List<Album>>
}