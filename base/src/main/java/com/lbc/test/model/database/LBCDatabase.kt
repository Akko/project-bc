package com.lbc.test.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.lbc.test.model.database.dao.AlbumDao
import com.lbc.test.model.database.dao.SongDao
import com.lbc.test.model.entity.Album
import com.lbc.test.model.entity.Song


/**
 * Created by --J.
 */
@Database(entities = [Album::class, Song::class], version = 1) // version 1
@TypeConverters(DatabaseConverters::class)
abstract class LBCDatabase : RoomDatabase() {

	// ===========================================================
	// Companion
	// ===========================================================

	companion object {

		const val DATABASE_NAME = "database-lbc"
	}


	// ===========================================================
	// DAO Entity
	// ===========================================================

	abstract fun albumDao(): AlbumDao

	abstract fun songDao(): SongDao
}