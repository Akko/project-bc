package com.lbc.test.model.entity.extension

import com.lbc.test.model.entity.Song
import com.lbc.test.util.isDebug


/**
 * Created by --J.
 */
fun Song.toTitle(): String = "$title${if (isDebug()) " ($songId)" else ""}"
