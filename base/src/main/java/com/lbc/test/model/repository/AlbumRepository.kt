package com.lbc.test.model.repository

import androidx.lifecycle.LiveData
import com.lbc.test.model.api.Resource
import com.lbc.test.model.entity.Album
import com.lbc.test.model.repository.expand.AlbumExpand


/**
 * Created by --J.
 */
interface AlbumRepository {

	// ===========================================================
	// Album
	// ===========================================================

	fun observeAlbums(): LiveData<Resource<List<Album>>>

	fun getAlbums(lastAlbumId: Int? = null, expand: AlbumExpand)

}