package com.lbc.test.model.repository.expand


/**
 * Created by --J.
 */
class AlbumExpand(
		songs: SongExpand? = null,
		override val name: String = ALBUMS,
		override val limit: Int? = null
) : Expand {

	companion object {
		const val ALBUMS = "albums"
	}

	override val list: ArrayList<Expand?> = arrayListOf(songs)
}