package com.lbc.test.model.repository.expand


/**
 * Created by --J.
 */
interface Expand {
	val list: ArrayList<Expand?>

	val name: String
	val limit: Int?
}