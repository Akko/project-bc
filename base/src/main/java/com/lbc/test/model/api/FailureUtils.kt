package com.lbc.test.model.api

import android.content.Context
import com.lbc.test.model.entity.Error
import com.lbc.test.base.R

/**
 * Created by --J.
 */
// ===========================================================
// HTTP Code
// ===========================================================

fun Error?.convertToText(context: Context?): String {
	context ?: return ""
	if (this == null) return ""
	return context.getString(R.string.problem_api_code_x, code.toString())
}