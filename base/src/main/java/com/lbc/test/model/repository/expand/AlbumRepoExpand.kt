package com.lbc.test.model.repository.expand


/**
 * Created by --J.
 */
object AlbumRepoExpand {

	// ===========================================================
	// Album
	// ===========================================================

	val EXPAND_TYPE_ALBUMS by lazy {
		AlbumExpand(
				songs = SongExpand()
		)
	}
}