package com.lbc.test.model.repository

import androidx.lifecycle.MediatorLiveData
import com.lbc.test.LIMIT_PAGE
import com.lbc.test.model.api.*
import com.lbc.test.model.database.LBCDatabase
import com.lbc.test.model.database.dao.AlbumDao
import com.lbc.test.model.database.dao.SongDao
import com.lbc.test.model.database.dao.queryAlbums
import com.lbc.test.model.database.dao.updateAlbumsAndDescendant
import com.lbc.test.model.entity.Album
import com.lbc.test.model.repository.expand.AlbumExpand
import com.lbc.test.model.service.AlbumService
import com.lbc.test.model.util.AlbumSort
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject


/**
 * Created by --J.
 */
class AlbumRepositoryImpl
@Inject constructor(
		database: LBCDatabase,
		retrofit: Retrofit,
		private val apiClient: ApiClient,
		private val appExecutors: AppExecutors
) : AlbumRepository {

	// ===========================================================
	// Property
	// ===========================================================

	// API
	private val service: AlbumService = retrofit.create(AlbumService::class.java)
	private val albumDao: AlbumDao = database.albumDao()
	private val songDao: SongDao = database.songDao()

	// LiveData
	private val albumsLiveData = MediatorLiveData<Resource<List<Album>>>()


	// ===========================================================
	// Album
	// ===========================================================

	override fun observeAlbums() = albumsLiveData

	override fun getAlbums(lastAlbumId: Int?, expand: AlbumExpand) {
		object : DataFetcher<List<Album>>(appExecutors, apiClient, albumsLiveData) {

			override fun fetch() = queryAlbums(lastAlbumId, albumDao, songDao, expand)

			override fun upsert(response: List<Album>) = updateAlbumsAndDescendant(response, albumDao, songDao, expand)

			override fun completeResource(resource: Resource<List<Album>>, data: List<Album>?) {
				resource.parameter = lastAlbumId
				resource.page = lastAlbumId
				resource.expand = expand
				// can calculate end if
				// -> RSuccess && data.size < LIMIT_PAGE
				// end if
				resource.end = resource.status == RSuccess && (data?.size ?: 0) < LIMIT_PAGE

				// order it if is mutable list
				(data as? ArrayList<Album>)?.let { AlbumSort.byAlbumSong(it) }
			}

			override fun getCall(): Call<List<Album>>? = if (lastAlbumId == null) service.getAlbums() else null
		}
	}
}