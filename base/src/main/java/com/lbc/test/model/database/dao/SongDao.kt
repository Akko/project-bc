package com.lbc.test.model.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.lbc.test.model.entity.Song


/**
 * Created by --J.
 */
@Dao
interface SongDao : BaseDao<Song> {

	// ===========================================================
	// Query
	// ===========================================================

	@Query("SELECT * FROM ${Song.DB_TABLE_NAME} WHERE ${Song.DB_KEY_ID} IN (:song_ids)")
	fun findSongsBySongIds(song_ids: List<Int>?): List<Song>?

	@Query("DELETE FROM ${Song.DB_TABLE_NAME}")
	fun nukeAll()
}