package com.lbc.test.injection

import com.lbc.test.model.repository.AlbumRepository
import com.lbc.test.model.repository.AlbumRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton


/**
 * Created by --J.
 */
@Module(includes = [ApiModule::class])
@Suppress("unused")
abstract class RepositoryModule {

	// ===========================================================
	// Repository
	// ===========================================================

	@Binds
	@Singleton
	abstract fun provideAlbumRepository(repo: AlbumRepositoryImpl): AlbumRepository
}