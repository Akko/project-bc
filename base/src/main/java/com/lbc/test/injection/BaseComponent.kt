package com.lbc.test.injection

import android.app.Application
import android.content.Context
import com.lbc.test.model.api.ApiClient
import com.lbc.test.model.api.AppExecutors
import com.lbc.test.model.database.LBCDatabase
import com.lbc.test.model.repository.AlbumRepository
import dagger.Component
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton


/**
 * Created by --J.
 */
@Singleton
@Component(modules = [ApiModule::class, RepositoryModule::class])
interface BaseComponent {

	// ===========================================================
	// Api Module
	// ===========================================================

	fun application(): Application

	fun applicationContext(): Context

	fun retrofit(): Retrofit

	fun httpClient(): OkHttpClient

	fun database(): LBCDatabase

	fun appExecutors(): AppExecutors

	fun apiClient(): ApiClient


	// ===========================================================
	// Repository Module
	// ===========================================================

	fun albumRepo(): AlbumRepository
}