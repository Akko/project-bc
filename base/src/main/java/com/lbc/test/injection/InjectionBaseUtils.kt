package com.lbc.test.injection

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders


/**
 * Created by --J.
 */
inline fun <reified T : ViewModel> AppCompatActivity.withActivityLifeCycle(factory: ViewModelProvider.Factory) = ViewModelProviders.of(this, factory).get(T::class.java)