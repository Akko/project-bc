package com.lbc.test.injection

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.lbc.test.API_BASE_URL
import com.lbc.test.model.api.ApiClient
import com.lbc.test.model.api.AppExecutors
import com.lbc.test.model.api.JsonConverterFactory
import com.lbc.test.model.database.LBCDatabase
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


/**
 * Created by --J.
 */
@Module
class ApiModule(private val application: Application) {

	// ===========================================================
	// Module
	// ===========================================================

	@Provides
	fun provideAppContext(): Context {
		return application
	}

	@Provides
	fun provideApplication(): Application {
		return application
	}


	// ===========================================================
	// Thread
	// ===========================================================

	@Provides
	@Singleton
	fun provideAppExecutors(): AppExecutors {
		return AppExecutors()
	}


	// ===========================================================
	// Network
	// ===========================================================

	@Provides
	@Singleton
	fun provideOkHttpClient(): OkHttpClient {
		val b = OkHttpClient.Builder()

		val timeOutInSeconds = 20L

		// Stetho to the rescue
		b.addNetworkInterceptor(StethoInterceptor())

		b.connectTimeout(timeOutInSeconds, TimeUnit.SECONDS)
		b.readTimeout(timeOutInSeconds, TimeUnit.SECONDS)
		b.writeTimeout(timeOutInSeconds, TimeUnit.SECONDS)

		return b.build()
	}

	@Provides
	@Singleton
	fun provideRetrofit(client: OkHttpClient): Retrofit {
		return Retrofit.Builder()
				.baseUrl(API_BASE_URL)
				.addConverterFactory(JsonConverterFactory)
				.client(client)
				.build()
	}


	// ===========================================================
	// Client
	// ===========================================================

	@Provides
	@Singleton
	fun provideApiClient(): ApiClient {
		return ApiClient()
	}


	// ===========================================================
	// Database
	// ===========================================================

	@Provides
	@Singleton
	fun provideDatabase(context: Context): LBCDatabase {
		return Room.databaseBuilder(context, LBCDatabase::class.java, LBCDatabase.DATABASE_NAME)
				.build()
	}
}