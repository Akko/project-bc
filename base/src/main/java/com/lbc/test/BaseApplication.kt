package com.lbc.test

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.stetho.Stetho
import com.lbc.test.base.BuildConfig
import com.lbc.test.injection.ApiModule
import com.lbc.test.injection.BaseComponent
import com.lbc.test.injection.DaggerBaseComponent


/**
 * Created by --J.
 */
open class BaseApplication : Application() {

	lateinit var appComponent: BaseComponent

	override fun onCreate() {
		super.onCreate()

		// DB && requests logs
		if (BuildConfig.DEBUG) {
			Stetho.initializeWithDefaults(this)
		}

		// DI
		appComponent = DaggerBaseComponent.builder()
				.apiModule(ApiModule(this))
				.build()

		// Enable VD
		AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

		// Enable Day Night themes
		AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
	}
}