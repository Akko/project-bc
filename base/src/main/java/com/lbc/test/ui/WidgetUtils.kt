package com.lbc.test.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by --J.
 */
fun ViewGroup.inflate(layoutRes: Int): View {
	return LayoutInflater.from(context).inflate(layoutRes, this, false)
}