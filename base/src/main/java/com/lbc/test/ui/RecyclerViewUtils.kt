package com.lbc.test.ui

import android.view.View
import android.view.ViewGroup
import androidx.annotation.IntDef
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import com.lbc.test.base.R


/**
 * Created by --J.
 */
// ===========================================================
// Extensions
// ===========================================================

fun RecyclerView?.firstChildTop() = if (this == null || this.childCount == 0) 0 else this[0].top

fun RecyclerView.doOnScrolled(action: (recyclerView: RecyclerView?, dx: Int, dy: Int) -> Unit) = addListener(onScrolled = action)

fun RecyclerView.doOnScrollStateChanged(action: (recyclerView: RecyclerView?, newState: Int) -> Unit) = addListener(onScrollStateChanged = action)

fun RecyclerView.addListener(
		onScrolled: ((recyclerView: RecyclerView?, dx: Int, dy: Int) -> Unit)? = null,
		onScrollStateChanged: ((recyclerView: RecyclerView?, newState: Int) -> Unit)? = null
): RecyclerView.OnScrollListener {
	val listener = object : RecyclerView.OnScrollListener() {

		override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
			onScrolled?.invoke(recyclerView, dx, dy)
		}

		override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
			onScrollStateChanged?.invoke(recyclerView, newState)
		}
	}
	addOnScrollListener(listener)
	return listener
}


// ===========================================================
// Dynamic Cell
// ===========================================================

@IntDef(ADAPTER_TYPE_DIVIDER_FULL,
		ADAPTER_TYPE_DIVIDER_MARGIN)
@Retention(AnnotationRetention.SOURCE)
@Suppress("unused")
internal annotation class DynamicCell

// int
const val ADAPTER_TYPE_DIVIDER_FULL = 0x21
const val ADAPTER_TYPE_DIVIDER_MARGIN = 0x22

// inflater
fun inflateDividerFull(parent: ViewGroup): RecyclerView.ViewHolder {
	val v = parent.inflate(R.layout.fragment_cell_vh_divider)
	return DividerViewHolder(v)
}

fun inflateDividerMargin(parent: ViewGroup): RecyclerView.ViewHolder {
	val v = parent.inflate(R.layout.fragment_cell_vh_divider_margin)
	return DividerMarginViewHolder(v)
}

// view holder
class DividerMarginViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view)

class DividerViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view)