package com.lbc.test.ui

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.make
import com.lbc.test.base.R
import com.lbc.test.model.api.convertToText
import com.lbc.test.model.entity.Error
import com.lbc.test.model.entity.Error.Companion.ERROR_TYPE_API
import com.lbc.test.model.entity.Error.Companion.ERROR_TYPE_NETWORK
import com.lbc.test.model.entity.Error.Companion.ERROR_TYPE_UNKNOWN


/**
 * Created by --J.
 */
fun snack(viewToSnack: View?, error: Error?, actionText: String? = null, action: (() -> Unit)? = null) {
	error ?: return
	viewToSnack ?: return

	when (error.type) {
		ERROR_TYPE_NETWORK -> {
			make(viewToSnack, viewToSnack.context.getString(R.string.problem_internet_connection), Snackbar.LENGTH_LONG).apply {
				snackText(view)?.setTextColor(Color.WHITE)
				show()
			}
		}
		ERROR_TYPE_API -> {
			make(viewToSnack, error.convertToText(viewToSnack.context), Snackbar.LENGTH_LONG).apply {
				view.setBackgroundColor(ContextCompat.getColor(context, R.color.red))
				snackText(view)?.setTextColor(Color.WHITE)
				if (actionText != null) {
					setActionTextColor(Color.WHITE)
					setAction(actionText) {
						action?.invoke()
					}
				}
				show()
			}
		}
		ERROR_TYPE_UNKNOWN -> {
			make(viewToSnack, viewToSnack.context.getString(R.string.problem_unknown), Snackbar.LENGTH_LONG).apply {
				snackText(view)?.setTextColor(Color.WHITE)
				show()
			}
		}
	}
}

@Suppress("NOTHING_TO_INLINE")
private inline fun snackText(view: View) = view.findViewById(com.google.android.material.R.id.snackbar_text) as? TextView