package com.lbc.test.ui.list

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lbc.test.base.R
import com.lbc.test.ui.inflate

/**
 * Created by --J.
 */
interface ErrorViewHolder {

	// interface
	interface ErrorListener {

		fun onClickError()
	}


	// error
	val retryTextView: TextView
	val infoTextView: TextView

	// onCreateViewHolder
	fun createError(listener: ErrorListener) {
		retryTextView.setOnClickListener {
			listener.onClickError()
		}
	}

	class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view), ErrorViewHolder {
		override val retryTextView: TextView by lazy { view.findViewById<TextView>(R.id.text_retry) }
		override val infoTextView: TextView by lazy { view.findViewById<TextView>(R.id.text_info) }
	}


	companion object {

		fun inflate(parent: ViewGroup): RecyclerView.ViewHolder {
			val v = parent.inflate(R.layout.fragment_cell_vh_error)
			return ViewHolder(v)
		}
	}
}