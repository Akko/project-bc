package com.lbc.test.ui.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lbc.test.base.R
import com.lbc.test.ui.inflate


/**
 * Created by --J.
 */
interface LoadViewHolder {

	class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view), LoadViewHolder

	companion object {

		fun inflate(parent: ViewGroup): RecyclerView.ViewHolder {
			val v = parent.inflate(R.layout.fragment_cell_vh_load)
			return ViewHolder(v)
		}
	}
}