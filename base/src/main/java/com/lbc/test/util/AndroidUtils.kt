package com.lbc.test.util

import android.os.Build
import com.lbc.test.base.BuildConfig

/**
 * Created by --J.
 */
fun isJellyBeanMR1(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1

fun isDebug() = BuildConfig.DEBUG