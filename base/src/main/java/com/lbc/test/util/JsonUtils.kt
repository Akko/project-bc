package com.lbc.test.util

import org.json.JSONException
import org.json.JSONObject


/**
 * Created by --J.
 */
fun JSONObject.getJSONString(key: String): String? {
	try {
		if (has(key) && !isNull(key)) {
			return getString(key)
		}
	} catch (e: JSONException) {
		e.printStackTrace()
	}
	return null
}

fun JSONObject.getJSONInt(key: String): Int? {
	try {
		if (has(key) && !isNull(key)) {
			return getInt(key)
		}
	} catch (e: JSONException) {
		e.printStackTrace()
	}

	return null
}