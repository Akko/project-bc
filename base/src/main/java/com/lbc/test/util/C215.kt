package com.lbc.test.util

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.lbc.test.base.R

/**
 * Created by --J.
 */
object C215 {

	private val defaultTransition = DrawableTransitionOptions.withCrossFade()

	fun with(context: Context, url: String?, requests: RequestOptions? = null, listener: RequestListener<Drawable>? = null, transitions: DrawableTransitionOptions = defaultTransition, imageView: ImageView) {
		if (!isValidContextForGlide(context)) return
		val glide = Glide.with(context)
				.load(url)
				.transition(transitions)

		if (requests != null) glide.apply(requests)
		if (listener != null) glide.listener(listener)

		glide.into(imageView)
	}


	// ===========================================================
	// Picture
	// ===========================================================

	fun forPictureSmallRadius(url: String?, imageView: ImageView) {
		val context = imageView.context
		if (!isValidContextForGlide(context)) return
		val radius = context.getSmallCornerRadius()
		val border = context.getSmallBorder()
		val grey = context.getGrey()

		val transforms = MultiTransformation(CenterCrop(), RoundedCornersTransformation(radius, color = grey, border = border))
		val request = RequestOptions.bitmapTransform(transforms)
				.placeholder(R.drawable.placeholer_grey_small_stroke_small_radius)
				.error(R.drawable.placeholer_error_small_stroke_small_radius)

		Glide.with(context)
				.load(url)
				.transition(defaultTransition)
				.apply(request)
				.into(imageView)
	}


	// ===========================================================
	// Value
	// ===========================================================

	private fun isValidContextForGlide(context: Context): Boolean {
		val activity = context as? Activity ?: return true
		val destroyState = (if (isJellyBeanMR1()) activity.isDestroyed else true)
		val killState = activity.isFinishing
		if (destroyState || killState) return false
		return true
	}

	private fun Context.getGrey() = ContextCompat.getColor(this, R.color.grey_divider)

	@Suppress("unused")
	private fun Context.getWhite() = Color.WHITE

	@Suppress("unused")
	private fun Context.getTransparent() = Color.TRANSPARENT

	private fun Context.getSmallBorder() = resources.getDimensionPixelSize(R.dimen.stroke_small_image).toFloat()

	private fun Context.getSmallCornerRadius() = resources.getDimensionPixelSize(R.dimen.corner_small_radius)


}